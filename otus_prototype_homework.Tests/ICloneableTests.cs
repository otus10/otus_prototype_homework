﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Xunit;

namespace otus_prototype_homework.Tests
{
    public class ICloneableTests
    {
        [Theory]
        [MemberData(nameof(Data_ICloneableObjects_AreExpected))]
        [Trait("IMyCloneable", "ICloneableObjects")]
        public void Clone_ICloneable_EqualAreExpected(ICloneable tested, object expected)
        {
            string actualSerialized = JsonSerializer.Serialize(tested.Clone());
            string expectedSerialized = JsonSerializer.Serialize(expected);
            Assert.Equal(expectedSerialized, actualSerialized);
        }

        public static IEnumerable<object[]> Data_ICloneableObjects_AreExpected()
        {
            yield return new object[]
            {
                new Truck(TruckType.Refregerator, 44m, TransportType.LandVehicle),
                new Truck(TruckType.Refregerator, 44m, TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new Truck(TruckType.Tent, 66m, TransportType.LandVehicle),
                new Truck(TruckType.Tent, 66m, TransportType.LandVehicle),
            };

            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.LandVehicle),
                new CargoTransport(44m, TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.Aviation),
                new CargoTransport(44m, TransportType.Aviation)
            };

            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.SeaTransport),
                new CargoTransport(44m, TransportType.SeaTransport)
            };
            yield return new object[]
           {
                new PassengerTransport( 30, TransportType.LandVehicle),
                new PassengerTransport(30, TransportType.LandVehicle)
           };

            yield return new object[]
            {
                new PassengerTransport( 110, TransportType.Aviation),
                new PassengerTransport(110, TransportType.Aviation)
            };

            yield return new object[]
            {
                new PassengerTransport(3000, TransportType.SeaTransport),
                new PassengerTransport(3000, TransportType.SeaTransport)
            };

            yield return new object[]
            {
                new Transport(TransportType.LandVehicle),
                new Transport(TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new Transport(TransportType.Aviation),
                new Transport(TransportType.Aviation)
            };

            yield return new object[]
            {
                new Transport(TransportType.SeaTransport),
                new Transport(TransportType.SeaTransport)
            };
        }
    }
}
