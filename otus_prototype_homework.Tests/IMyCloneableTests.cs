using System;
using System.Collections.Generic;
using System.Text.Json;
using Xunit;

namespace otus_prototype_homework.Tests
{
    public class IMyCloneableTests
    {
        [Theory]
        [MemberData(nameof(Data_TruckObjects_AreExpected))]
        [Trait("IMyCloneable", "Truck")]
        public void Clone_Truck_EqualAreExpected(Truck tested, Truck expected)
        {

            string actualSerialized = JsonSerializer.Serialize(tested.Clone());
            string expectedSerialized = JsonSerializer.Serialize(expected);
            Assert.Equal(expectedSerialized, actualSerialized);
        }
        public static IEnumerable<object[]> Data_TruckObjects_AreExpected()
        {
            yield return new object[]
            {
                new Truck(TruckType.Refregerator, 44m, TransportType.LandVehicle),
                new Truck(TruckType.Refregerator, 44m, TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new Truck(TruckType.Tent, 66m, TransportType.LandVehicle),
                new Truck(TruckType.Tent, 66m, TransportType.LandVehicle),
            };
        }

        [Theory]
        [MemberData(nameof(Data_CargoTransport_AreExpected))]
        [Trait("IMyCloneable", "CargoTransport")]
        public void Clone_CargoTransport_EqualAreExpected(CargoTransport tested, CargoTransport expected)
        {

            string actualSerialized = JsonSerializer.Serialize(tested.Clone());
            string expectedSerialized = JsonSerializer.Serialize(expected);
            Assert.Equal(expectedSerialized, actualSerialized); ;
        }
        public static IEnumerable<object[]> Data_CargoTransport_AreExpected()
        {
            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.LandVehicle),
                new CargoTransport(44m, TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.Aviation),
                new CargoTransport(44m, TransportType.Aviation)
            };

            yield return new object[]
            {
                new CargoTransport( 44m, TransportType.SeaTransport),
                new CargoTransport(44m, TransportType.SeaTransport)
            };
        }

        [Theory]
        [MemberData(nameof(Data_PassengerTransport_AreExpected))]
        [Trait("IMyCloneable", "PassengerTransport")]
        public void Clone_PassengerTransport_EqualAreExpected(PassengerTransport tested, PassengerTransport expected)
        {

            string actualSerialized = JsonSerializer.Serialize(tested.Clone());
            string expectedSerialized = JsonSerializer.Serialize(expected);
            Assert.Equal(expectedSerialized, actualSerialized);
        }
        public static IEnumerable<object[]> Data_PassengerTransport_AreExpected()
        {
            yield return new object[]
            {
                new PassengerTransport( 30, TransportType.LandVehicle),
                new PassengerTransport(30, TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new PassengerTransport( 110, TransportType.Aviation),
                new PassengerTransport(110, TransportType.Aviation)
            };

            yield return new object[]
            {
                new PassengerTransport(3000, TransportType.SeaTransport),
                new PassengerTransport(3000, TransportType.SeaTransport)
            };
        }

        [Theory]
        [MemberData(nameof(Data_Transport_AreExpected))]
        [Trait("IMyCloneable", "Transport")]
        public void Clone_Transport_EqualAreExpected(Transport tested, Transport expected)
        {

            string actualSerialized = JsonSerializer.Serialize(tested.Clone());
            string expectedSerialized = JsonSerializer.Serialize(expected);
            Assert.Equal(expectedSerialized, actualSerialized);
        }
        public static IEnumerable<object[]> Data_Transport_AreExpected()
        {
            yield return new object[]
            {
                new Transport(TransportType.LandVehicle),
                new Transport(TransportType.LandVehicle)
            };

            yield return new object[]
            {
                new Transport(TransportType.Aviation),
                new Transport(TransportType.Aviation)
            };

            yield return new object[]
            {
                new Transport(TransportType.SeaTransport),
                new Transport(TransportType.SeaTransport)
            };
        }

        [Fact]
        [Trait("IMyCloneable", "Polymorphysm_Truck")]
        public void Clone_Polymorphysm_Truck_EqualAreExpected()
        {
            Transport transport = new Truck(TruckType.Tent, 66m, TransportType.LandVehicle);
            string originalType = transport.GetType().Name;
            object clone = transport.Clone();
            string cloneType = clone.GetType().Name;
            Assert.Equal(originalType, cloneType);
        }

        [Fact]
        [Trait("IMyCloneable", "Polymorphysm_CargoTransport")]
        public void Clone_Polymorphysm_CargoTransport_EqualAreExpected()
        {
            Transport transport = new CargoTransport(44m, TransportType.Aviation);
            string originalType = transport.GetType().Name;
            object clone = transport.Clone();
            string cloneType = clone.GetType().Name;
            Assert.Equal(originalType, cloneType);
        }

        [Fact]
        [Trait("IMyCloneable", "Polymorphysm_PassengerTransport")]
        public void Clone_Polymorphysm_PassengerTransport_EqualAreExpected()
        {
            Transport transport = new PassengerTransport(3000, TransportType.SeaTransport);
            string originalType = transport.GetType().Name;
            object clone = transport.Clone();
            string cloneType = clone.GetType().Name;
            Assert.Equal(originalType, cloneType);
        }
    }
}

