﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_prototype_homework
{
    public class PassengerTransport : Transport, IMyCloneable<PassengerTransport>, ICloneable
    {
        public int PassengerCapacity { get; set; }

        public PassengerTransport(int passengerCapacity, TransportType transportType) : base(transportType)
        {
            PassengerCapacity = passengerCapacity;
        }
        public override PassengerTransport Clone()
        {
            return new PassengerTransport(PassengerCapacity, TransportType);
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
