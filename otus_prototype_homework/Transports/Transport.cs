﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_prototype_homework
{
    public class Transport : IMyCloneable<Transport>, ICloneable
    {
        public TransportType TransportType{ get; set; }
        public Transport(TransportType transportType)
        {
            TransportType = transportType;
        }
        public virtual Transport Clone()
        {
            return new Transport(TransportType);
        }
        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
