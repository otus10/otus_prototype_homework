﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_prototype_homework
{
    public class CargoTransport : Transport, IMyCloneable<CargoTransport>, ICloneable
    {
        public decimal CapacityVolume { get; set; }

        public CargoTransport(decimal capacityVolume ,TransportType transportType) : base(transportType)    
        {
            CapacityVolume = capacityVolume;
        }
        public override CargoTransport Clone()
        {
            return new CargoTransport(CapacityVolume, TransportType);    
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
