﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_prototype_homework
{
    public class Truck : CargoTransport, IMyCloneable<Truck>, ICloneable
    {
        public TruckType TruckType { get; set; }

        public Truck(TruckType truckType, decimal capacityVolume, TransportType transportType) : base(capacityVolume, transportType)
        {
            TruckType = truckType;
        }
        public override Truck Clone()
        {
            return new Truck(TruckType, CapacityVolume, TransportType);
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
